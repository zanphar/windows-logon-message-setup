# README #

This software provides you with the ability to setup a custom message that is presented to users of your system before they may log in. 
The software is written in Visual Basic .NET 2013 against the Microsoft .NET Framework 4.5.


## New Features ##

1. You can now select a file to be used for your message instead of manually typing it out or copying and pasting your message, and is especially much faster in an environment with more than one machine.

2. You can now use a "pre-defined" title for the message, instead of typing it out by hand.

3. Added two files with generic text messages in Rich Text Format (RTF) and Plain Text Format (TXT). They are located in the Resources directory.

## Changes ##

1. (Fix) - Windows assumed Windows Logon Message Setup was an installation program, and would cause an installation failure message to appear. This has been corrected.



