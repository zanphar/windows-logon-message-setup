﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class About
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.abPic = New System.Windows.Forms.PictureBox()
        Me.LabelVersion = New System.Windows.Forms.Label()
        Me.LabelCopyright = New System.Windows.Forms.Label()
        Me.onOK = New System.Windows.Forms.Button()
        CType(Me.abPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'abPic
        '
        Me.abPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.abPic.Image = Global.Windows_Logon_Message_Setup.My.Resources.Resources.Windows_Logon_Message_Setup
        Me.abPic.Location = New System.Drawing.Point(12, 12)
        Me.abPic.Name = "abPic"
        Me.abPic.Size = New System.Drawing.Size(380, 37)
        Me.abPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.abPic.TabIndex = 0
        Me.abPic.TabStop = False
        '
        'LabelVersion
        '
        Me.LabelVersion.BackColor = System.Drawing.Color.Transparent
        Me.LabelVersion.Location = New System.Drawing.Point(12, 57)
        Me.LabelVersion.Name = "LabelVersion"
        Me.LabelVersion.Size = New System.Drawing.Size(380, 23)
        Me.LabelVersion.TabIndex = 1
        Me.LabelVersion.Text = "Product Version..."
        Me.LabelVersion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LabelCopyright
        '
        Me.LabelCopyright.Location = New System.Drawing.Point(12, 80)
        Me.LabelCopyright.Name = "LabelCopyright"
        Me.LabelCopyright.Size = New System.Drawing.Size(380, 30)
        Me.LabelCopyright.TabIndex = 2
        Me.LabelCopyright.Text = "Copyright Information..."
        Me.LabelCopyright.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'onOK
        '
        Me.onOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.onOK.Location = New System.Drawing.Point(317, 117)
        Me.onOK.Name = "onOK"
        Me.onOK.Size = New System.Drawing.Size(75, 23)
        Me.onOK.TabIndex = 3
        Me.onOK.Text = "&OK"
        Me.onOK.UseVisualStyleBackColor = True
        '
        'About
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(404, 152)
        Me.Controls.Add(Me.onOK)
        Me.Controls.Add(Me.LabelCopyright)
        Me.Controls.Add(Me.LabelVersion)
        Me.Controls.Add(Me.abPic)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "About"
        Me.Padding = New System.Windows.Forms.Padding(9)
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "About"
        CType(Me.abPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents abPic As System.Windows.Forms.PictureBox
    Friend WithEvents LabelVersion As System.Windows.Forms.Label
    Friend WithEvents LabelCopyright As System.Windows.Forms.Label
    Friend WithEvents onOK As System.Windows.Forms.Button

End Class
