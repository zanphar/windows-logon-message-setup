This is a general message that is included with the Windows Logon Message Setup software that is developed by Charles M. McDonald.
There are two versions of this file, one that is available in Rich Text Format (RTF) and in Plain Text Format (TXT) for you to choose from. These are just samples with no important information.
The purpose,   you use Windows Logon Message Setup software to select these files, then you can type in a title or chose from a pre-defined title value, and click save. When you logout of your account, you should see the message before you�re able to login again.

