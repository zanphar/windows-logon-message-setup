﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Windows Logon Message Setup")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Charles M. McDonald")> 
<Assembly: AssemblyProduct("Windows Logon Message Setup")> 
<Assembly: AssemblyCopyright("Copyright © 2014-2015 Charles M. McDonald")> 
<Assembly: AssemblyTrademark("All rights reserved.")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("6690a727-b297-4034-9faf-f4da6c52aad7")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.*")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
