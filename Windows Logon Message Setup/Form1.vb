﻿' Windows Logon Message Setup
' 
' This software has been restricted based upon the limitations imposed by the
' Microsoft® Windows® platform. These limitations are as follows:
' 
'  1. The notification dialog (or the "Caption") is limited to 80 characters.
'  2. The actual message is limited to 2,048 characters if written in the
'     form of paragraphics (512 characters each paragraph.)
' 
' The Erase button has been implemented to allow you to quickly wipeout the
' caption and message without pressing the Save button.
' 
' THIS SOFTWARE IS PROVIDED "AS-IS" WITHOUT ANY GUARANTEE OR WARRANTY WHETHER EXPRESS
' OR IMPLIED, INCLUDING BUT NOT LIMITED TO MERCHANTABILITY OR FITNESS FOR ANY
' PARTICULAR PURPOSE. IN NO EVENT SHALL THE AUTHOR OF THIS SOFTWARE BE HELD LIABLE
' OR RESPONSIBLE FOR ANY USE OF THIS SOFTWARE.
' 
' Copyright © 1992-2015 Charles M. McDonald. All rights reserved.

Public Class frmMain

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        inNotice.MaxLength = 80
        inMessage.MaxLength = 2048

        Try
            inNotice.Text = (My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\", "legalnoticecaption", Text))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            inMessage.Text = (My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\", "legalnoticetext", Text))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub onOK_Click(sender As Object, e As EventArgs) Handles onOK.Click

        Application.Exit()

    End Sub

    Private Sub Form1_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing

        Try

            Dim result As DialogResult = MessageBox.Show("Any information that you entered will be lost if it is not saved!" & vbCrLf & _
                                                         "Are you sure that you wish to continue?" & vbCrLf & _
                                                         "", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If result = Windows.Forms.DialogResult.No Then

                e.Cancel = True

            Else

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub onSave_Click(sender As Object, e As EventArgs) Handles onSave.Click

        Try

            inNotice.Text = ComboBox1.SelectedItem

        Catch ex As Exception

            MsgBox(ex.Message)

        End Try

        Try
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\", "legalnoticecaption", "" & inNotice.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\", "legalnoticetext", "" & inMessage.Text)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            inNotice.Refresh()
            inMessage.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub onErase_Click(sender As Object, e As EventArgs) Handles onErase.Click

        Try
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\", "legalnoticecaption", "" & inNotice.Text)
            inNotice.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\", "legalnoticetext", "" & inMessage.Text)
            inMessage.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            inNotice.Refresh()
            inMessage.Refresh()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub onAbout_Click(sender As Object, e As EventArgs) Handles onAbout.Click

        Try
            About.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub onOpen_Click(sender As Object, e As EventArgs) Handles onOpen.Click

        Try
            OpenFileDialog1.Filter = "Rich Text Format (*.rtf) |*.rtf;*.rtf|Plain Text Format (*.txt) |*.txt|All Files (*.*) |*.*"
            OpenFileDialog1.FileName = ""
            OpenFileDialog1.FilterIndex = 2
            OpenFileDialog1.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
            OpenFileDialog1.ShowDialog()
            Me.inMessage.Text = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged

        Try
            inNotice.Text = ComboBox1.SelectedItem
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

End Class
