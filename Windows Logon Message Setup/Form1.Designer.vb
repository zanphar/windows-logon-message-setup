﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.onOK = New System.Windows.Forms.Button()
        Me.onSave = New System.Windows.Forms.Button()
        Me.inMessage = New System.Windows.Forms.RichTextBox()
        Me.inNotice = New System.Windows.Forms.TextBox()
        Me.lblNotice = New System.Windows.Forms.Label()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.onErase = New System.Windows.Forms.Button()
        Me.onAbout = New System.Windows.Forms.Button()
        Me.uphld = New System.Windows.Forms.Label()
        Me.lbIntro = New System.Windows.Forms.Label()
        Me.appPic = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.onOpen = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        CType(Me.appPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'onOK
        '
        Me.onOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.onOK.Location = New System.Drawing.Point(591, 507)
        Me.onOK.Name = "onOK"
        Me.onOK.Size = New System.Drawing.Size(87, 19)
        Me.onOK.TabIndex = 0
        Me.onOK.Text = "&Close"
        Me.onOK.UseVisualStyleBackColor = True
        '
        'onSave
        '
        Me.onSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.onSave.Location = New System.Drawing.Point(498, 507)
        Me.onSave.Name = "onSave"
        Me.onSave.Size = New System.Drawing.Size(87, 19)
        Me.onSave.TabIndex = 1
        Me.onSave.Text = "&Save"
        Me.onSave.UseVisualStyleBackColor = True
        '
        'inMessage
        '
        Me.inMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.inMessage.Location = New System.Drawing.Point(12, 186)
        Me.inMessage.Name = "inMessage"
        Me.inMessage.Size = New System.Drawing.Size(668, 252)
        Me.inMessage.TabIndex = 3
        Me.inMessage.Text = ""
        '
        'inNotice
        '
        Me.inNotice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.inNotice.Location = New System.Drawing.Point(12, 135)
        Me.inNotice.Name = "inNotice"
        Me.inNotice.Size = New System.Drawing.Size(668, 18)
        Me.inNotice.TabIndex = 4
        Me.inNotice.WordWrap = False
        '
        'lblNotice
        '
        Me.lblNotice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNotice.Location = New System.Drawing.Point(10, 105)
        Me.lblNotice.Name = "lblNotice"
        Me.lblNotice.Size = New System.Drawing.Size(670, 27)
        Me.lblNotice.TabIndex = 5
        Me.lblNotice.Text = "Please enter the title of your announcement or notification prior to logging on t" & _
    "o the system. The maximum amount of characters allowed is 80."
        '
        'lblMessage
        '
        Me.lblMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMessage.Location = New System.Drawing.Point(10, 156)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(668, 27)
        Me.lblMessage.TabIndex = 6
        Me.lblMessage.Text = "The message that you provide here will be shown under the caption title you typed" & _
    " above before the user may logon to the system."
        '
        'onErase
        '
        Me.onErase.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.onErase.Location = New System.Drawing.Point(405, 507)
        Me.onErase.Name = "onErase"
        Me.onErase.Size = New System.Drawing.Size(87, 19)
        Me.onErase.TabIndex = 7
        Me.onErase.Text = "&Erase"
        Me.onErase.UseVisualStyleBackColor = True
        '
        'onAbout
        '
        Me.onAbout.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.onAbout.Location = New System.Drawing.Point(12, 507)
        Me.onAbout.Name = "onAbout"
        Me.onAbout.Size = New System.Drawing.Size(87, 19)
        Me.onAbout.TabIndex = 8
        Me.onAbout.Text = "&About"
        Me.onAbout.UseVisualStyleBackColor = True
        '
        'uphld
        '
        Me.uphld.BackColor = System.Drawing.Color.White
        Me.uphld.Dock = System.Windows.Forms.DockStyle.Top
        Me.uphld.Location = New System.Drawing.Point(0, 0)
        Me.uphld.Name = "uphld"
        Me.uphld.Size = New System.Drawing.Size(692, 84)
        Me.uphld.TabIndex = 10
        '
        'lbIntro
        '
        Me.lbIntro.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbIntro.BackColor = System.Drawing.Color.White
        Me.lbIntro.Location = New System.Drawing.Point(10, 9)
        Me.lbIntro.Name = "lbIntro"
        Me.lbIntro.Size = New System.Drawing.Size(555, 67)
        Me.lbIntro.TabIndex = 12
        Me.lbIntro.Text = "Windows Logon Message Setup allows you to create and customize messages that are " & _
    "displayed to users of your system before they can proceed to the logon page for " & _
    "this system." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'appPic
        '
        Me.appPic.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.appPic.Image = Global.Windows_Logon_Message_Setup.My.Resources.Resources.wizard
        Me.appPic.Location = New System.Drawing.Point(578, 9)
        Me.appPic.Name = "appPic"
        Me.appPic.Size = New System.Drawing.Size(100, 67)
        Me.appPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.appPic.TabIndex = 13
        Me.appPic.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'onOpen
        '
        Me.onOpen.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.onOpen.Location = New System.Drawing.Point(105, 507)
        Me.onOpen.Name = "onOpen"
        Me.onOpen.Size = New System.Drawing.Size(87, 19)
        Me.onOpen.TabIndex = 14
        Me.onOpen.Text = "&Open"
        Me.onOpen.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(10, 452)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(670, 27)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Alternately, you may choose a predefined title for the message you’ve chosen or m" & _
    "anually typed out. If you do not find one that suits your message, you’ll need t" & _
    "o manually enter a title." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'ComboBox1
        '
        Me.ComboBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Announcement", "General Announcement", "General Information", "Important Announcement", "Important Information", "Important Notice", "Information", "Maintenance", "Notice", "Restricted Access", "Restricted System", "Scheduled Maintenance"})
        Me.ComboBox1.Location = New System.Drawing.Point(12, 482)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(668, 19)
        Me.ComboBox1.Sorted = True
        Me.ComboBox1.TabIndex = 16
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 11.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(692, 538)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.onOpen)
        Me.Controls.Add(Me.appPic)
        Me.Controls.Add(Me.lbIntro)
        Me.Controls.Add(Me.uphld)
        Me.Controls.Add(Me.onAbout)
        Me.Controls.Add(Me.onErase)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.lblNotice)
        Me.Controls.Add(Me.inNotice)
        Me.Controls.Add(Me.inMessage)
        Me.Controls.Add(Me.onSave)
        Me.Controls.Add(Me.onOK)
        Me.Font = New System.Drawing.Font("Lucida Console", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Windows Logon Message Setup"
        CType(Me.appPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents onOK As System.Windows.Forms.Button
    Friend WithEvents onSave As System.Windows.Forms.Button
    Friend WithEvents inMessage As System.Windows.Forms.RichTextBox
    Friend WithEvents inNotice As System.Windows.Forms.TextBox
    Friend WithEvents lblNotice As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents onErase As System.Windows.Forms.Button
    Friend WithEvents onAbout As System.Windows.Forms.Button
    Friend WithEvents uphld As System.Windows.Forms.Label
    Friend WithEvents lbIntro As System.Windows.Forms.Label
    Friend WithEvents appPic As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents onOpen As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox

End Class
